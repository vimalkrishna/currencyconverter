<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<h2>Create New Account</h2>

<sf:form id="details" method="post"
	action="${pageContext.request.contextPath}/createaccount"
	commandName="user">

	<table class="formtable">
		<tr>
			<td class="label">Username:</td>
			<td><sf:input class="formTextSize" path="username" name="username"
					type="text" /><br />
				<div class="error">
					<sf:errors path="username"></sf:errors>
				</div></td>
		</tr>
		<tr>
			<td class="label">Name:</td>
			<td><sf:input class="formTextSize" path="name" name="name"
					type="text" /><br />
				<div class="error">
					<sf:errors path="name"></sf:errors>
				</div></td>
		</tr>
	
		<tr>
			<td class="label">Date Of Birth:</td>
			<td><sf:input class="formTextSize" path="dob" name="dob" id="dob" placeholder="DD-MM-YY"
					type="text" /><br />
				<div class="error">
					<sf:errors path="dob"></sf:errors>
				</div></td>
		</tr>
		<tr>
			<td class="label">Street:</td>
			<td><sf:input class="formTextSize" path="street" name="street"
					type="text" /><br />
				<div class="error">
					<sf:errors path="street"></sf:errors>
				</div></td>
		</tr>

		<tr>
			<td class="label">City:</td>
			<td><sf:input class="formTextSize" path="city" name="city"
					type="text" /><br />
				<div class="error">
					<sf:errors path="city"></sf:errors>
				</div></td>
		</tr>

		<tr>
			<td class="label">Zip:</td>
			<td><sf:input class="formTextSize" path="zip" name="zip" type="text" placeholder="12345" /><br />
				<div class="error">
					<sf:errors path="zip"></sf:errors>
				</div></td>
		</tr>

		<tr>
			<td class="label">Country:</td>
			<td><sf:select class="selectCountry" path="country">
					<option value="Australia">Australia</option>
					<option value="Austria">Austria</option>
					<option value="Brazil">Brazil</option>
					<option value="China">China</option>
					<option value="Denmark">Denmark</option>
					<option value="France">France</option>
					<option value="Germany">Germany</option>
					<option value="India">India</option>
					<option value="Japan">Japan</option>
					<option value="Netherlands">Netherlands</option>
					<option value="New Zealand">New Zealand</option>
					<option value="Spain">Spain</option>
					<option value="United Kingdom">United Kingdom</option>
					<option value="United States">United States</option>
				</sf:select></td>
		</tr>

		<tr>
			<td class="label">Email:</td>
			<td><sf:input class="formTextSize" path="email" name="email"
					type="text" />
				<div class="error">
					<sf:errors path="email"></sf:errors>
				</div></td>
		</tr>
		<tr>
			<td class="label">Password:</td>
			<td><sf:input id="password" class="formTextSize" path="password"
					name="password" type="password" />
				<div class="error">
					<sf:errors path="password"></sf:errors>
				</div></td>
		</tr>
		<tr>
			<td class="label">Confirm Password:</td>
			<td><input id="confirmpass" class="formTextSize" name="confirmpass"
				type="password" />
				<div id="matchpass"></div></td>
		</tr>
		<tr>
			<td class="label"></td>
			<td><input class="formTextSubmit" value="Create account" type="submit" /></td>
		</tr>
	</table>

</sf:form>
