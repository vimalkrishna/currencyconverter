<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
	$(document).ready(function() {
		document.f.j_username.focus();
	});
</script>

<h2>Login with Username and Password</h2>

<c:if test="${param.error != null}">

	<p class="error">Login failed. Please check your username and password.</p>
</c:if>


<form name='f'
	action='${pageContext.request.contextPath}/j_spring_security_check'
	method='POST'>
	<table class="formtable">
		<tr>
			<td class="label">User:</td>
			<td><input  class="formTextSize" type='text' name='j_username' value=''></td>
		</tr>
		<tr>
			<td class="label">Password:</td>
			<td><input  class="formTextSize" type='password' name='j_password' /></td>
		</tr>
		<tr>
			<td class="label">Remember me:</td>
			<td align="left">&nbsp;&nbsp;<input type='checkbox' name='_spring_security_remember_me'
				checked="checked" /></td>
		</tr>
		<tr>
			<td class="label"></td>
			<td><input class="formTextSubmit"  name="submit" type="submit" value="Login" /></td>
		</tr>
		<tr><td class="heightAdjust" colspan="2" class="label"></td></tr>
	</table>
</form>

<p>
	<a href="<c:url value="/newaccount"/>">Create new account</a>
</p>


