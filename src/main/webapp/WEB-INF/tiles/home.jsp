<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<section>
	<article id="tabs-centre" class="tabs">
		<ul>
			<li><a href="#tabs-centre-1" id="tab1">Live Rates</a></li>
			<li><a href="#tabs-centre-2" id="tab2">Historical Rates</a></li>
		</ul>
		<!-- Live section -->
		<div id="tabs-centre-1">
			<form id="currForm1">
				<table>
					<tr>
						<td width="3em">Amount</td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em">From Currency</td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em">To Currency</td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em">&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td width="3.15em"><input class="amount" id="amount"
							type="text" size="5" value="1"></td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em"><select id="fromCurrency"
							name="fromCurrency">
								<option value='EUR' title='Euro'>EUR - Euro</option>
								<option value='USD' title='US Dollar'>USD - US Dollar</option>
								<option value='GBP' title='Great Britain Pound'>GBP -
									British Pound</option>
								<option value='NZD' title='New Zealand Dollar'>NZD -
									New Zealand Dollar</option>
								<option value='AUD' title='Australian Dollar'>AUD -
									Australian Dollar</option>
								<option value='JPY' title='Japanese Yen'>JPY - Japanese
									YEN</option>
								<option value='HUF' title='Hungarian Frank'>HUF -
									Hungarian Forint</option>
								<option value='INR' title='Indian Rupee'>INR - Indian
									Rupee</option>
						</select></td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em"><select id="toCurrency" name="toCurrency">
								<option value='USD' title='US Dollar'>USD - US Dollar</option>
								<option value='EUR' title='Euro'>EUR - Euro</option>
								<option value='GBP' title='Great Britain Pound'>GBP -
									Brtish Pound</option>
								<option value='NZD' title='New Zealand Dollar'>NZD -
									New Zealand Dollar</option>
								<option value='AUD' title='Australian Dollar'>AUD -
									Australian Dollar</option>
								<option value='JPY' title='Japanese Yen'>JPY - Japanese
									YEN</option>
								<option value='HUF' title='Hungarian Frank'>HUF -
									Hungarian Forint</option>
								<option value='INR' title='Indian Rupee'>INR - Indian
									Rupee</option>
						</select></td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em"><input id="convert" type="Button"
							value="Convert"></td>
					</tr>
				</table>
			</form>
		</div>
		<!-- End of Live section -->
		<!-- Historical section -->
		<div id="tabs-centre-2">
			<form id="currForm2">
				<table>
					<tr>
						<td width="6.25em">Historical</td>
						<td>&nbsp;&nbsp;</td>
						<td width="3.15em">Amount</td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em">From Currency</td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em">To Currency</td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em">&nbsp;&nbsp;</td>
					</tr>
					<tr>
						<td width="6.25em"><input type="text" id="historical"
							size="12" readonly="readonly" style="background: white;"></td>
						<td>&nbsp;&nbsp;</td>
						<td width="3.15em"><input class="amount" id="amount1"
							type="text" size="5" value="1"></td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em"><select id="fromCurrency1"
							name="fromCurrency1">
								<option value='EUR' title='Euro'>EUR - Euro</option>
								<option value='USD' title='US Dollar'>USD - US Dollar</option>
								<option value='GBP' title='Great Britain Pound'>GBP -
									British Pound</option>
								<option value='NZD' title='New Zealand Dollar'>NZD -
									New Zealand Dollar</option>
								<option value='AUD' title='Australian Dollar'>AUD -
									Australian Dollar</option>
								<option value='JPY' title='Japanese Yen'>JPY - Japanese
									YEN</option>
								<option value='HUF' title='Hungarian Frank'>HUF -
									Hungarian Forint</option>
								<option value='INR' title='Indian Rupee'>INR - Indian
									Rupee</option>
						</select></td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em"><select id="toCurrency1"
							name="toCurrency1">
								<option value='USD' title='US Dollar'>USD - US Dollar</option>
								<option value='EUR' title='Euro'>EUR - Euro</option>
								<option value='GBP' title='Great Britain Pound'>GBP -
									British Pound</option>
								<option value='NZD' title='New Zealand Dollar'>NZD -
									New Zealand Dollar</option>
								<option value='AUD' title='Australian Dollar'>AUD -
									Australian Dollar</option>
								<option value='JPY' title='Japanese Yen'>JPY - Japanese
									YEN</option>
								<option value='HUF' title='Hungarian Frank'>HUF -
									Hungarian Forint</option>
								<option value='INR' title='Indian Rupee'>INR - Indian
									Rupee</option>
						</select></td>
						<td>&nbsp;&nbsp;</td>
						<td width="6.25em"><input id="convert1" type="Button" value="Convert"></td>
					</tr>
				</table>
			</form>
		</div>
		<div id="tabs-centre-1-result">
			<div id="tabs-centre-2-result"></div>
		</div>
		<!-- End of Historical section -->
	</article>
</section>
<script type="text/javascript">

	function getDateTime() {
		var now = new Date();
		var year = now.getFullYear();
		var month = now.getMonth() + 1;
		var day = now.getDate();
		var hour = now.getHours();
		var minute = now.getMinutes();
		var second = now.getSeconds();
		if (month.toString().length == 1) {
			var month = '0' + month;
		}
		if (day.toString().length == 1) {
			var day = '0' + day;
		}
		if (hour.toString().length == 1) {
			var hour = '0' + hour;
		}
		if (minute.toString().length == 1) {
			var minute = '0' + minute;
		}
		if (second.toString().length == 1) {
			var second = '0' + second;
		}
		var dateTime = year + '-' + month + '-' + day + ' ' + hour + ':'
				+ minute + ':' + second;
		return dateTime;
	}
</script>
<script type="text/javascript">
	$(document)
			.ready(
					function() {
						
						$('#convert')
								.click(
										function() {
											$("#dyTable").remove();
											var timestamp = getDateTime();

											var showData = $('#tabs-centre-2-result');
											var amount = document
													.getElementById("amount").value;
											var sourceCurr = document
													.getElementById("fromCurrency");
											var sCurr = sourceCurr.options[sourceCurr.selectedIndex].value;
											var endpoint = "live";
											// Live part
											$.getJSON(
															"http://apilayer.net/api/"
																	+ endpoint
																	+ "?access_key=bc589763e4de333cf044b8c881783651&currencies=EUR,USD,GBP,NZD,AUD,JPY,HUF,INR&source="
																	+ sCurr
																	+ "&format=1n",
															function(data) {
																console
																		.log(data);
																if (data.success == true) {

																	var fromCurr = document
																			.getElementById("fromCurrency");
																	var fCurr = fromCurr.options[fromCurr.selectedIndex].value;

																	var toCurr = document
																			.getElementById("toCurrency");
																	var tCurr = toCurr.options[toCurr.selectedIndex].value;

																	var str = fCurr
																			+ tCurr;
																	console
																			.log(str);
																	var jsonVal = "data.quotes";
																	var finalString = jsonVal
																			+ "."
																			+ str;
																	var finalCalculation = eval(finalString);
																	var finalAmount = amount
																			* finalCalculation;
																	var calculatedAmount = parseFloat(
																			Math
																					.round(finalAmount * 10000) / 10000)
																			.toFixed(
																					4);
																	var content = "";
																	content = "<table id='dyTable' cellpadding='1em'>"
																	content += '<tr><td>'
																			+ amount
																			+ '</td><td class="greenFontBold">'
																			+ fCurr
																			+ '</td><td>&nbsp; = &nbsp;</td><td>'
																			+ calculatedAmount
																			+ '</td><td class="greenFontBold">'
																			+ tCurr
																			+ '</td></tr>';
																	content += '<tr><td colspan="5" align="center">'
																			+ timestamp
																			+ '</td></tr>';
																	content += "</table>"
																	$(
																			'#tabs-centre-2-result')
																			.append(
																					content);

																	//showData.text(calculatedAmount);
																} else {
																	alert("Service unavailable");
																}
																 sendMessage(endpoint,timestamp,amount,fCurr,calculatedAmount,tCurr); 
															}); // end of $.getJSON	 
											// After client display, send data to the records table in the database															

										}); // end of click
					}); // end of ready function #tabs-centre

	//========================================================
// This is for real time record display after logged in user converts.					
function showRecords(data) {
		$("#generatedTable").html("");
		var content = "";
		content += '<table class="offers">'
		content += '<tr class="bold">'
		content += '<td width="120px">Type of request</td>'
		content += '<td width="200px">Date (ISO format)</td>'
		content += '<td width="100px" align="center">Amount</td>'
		content += '<td width="100px" align="center">From currency</td>'
		content += '<td width="100px" align="center">Amounts to</td>'
		content += '<td width="100px" align="center">To currency</td>'
		content += '</tr>'
		
		var toLoop = null;
		if(data.size < 10){
			toLoop = data.size;
		}else{
			toLoop = 10;
		}
		
		 for(var i=0; i < toLoop; i++) {
		 	var record = data.records[i];
				 	
			 content += '<tr><td class="request">'
			+ record.typeOfRequest
		 	+ '</td><td>'
			+ record.timeStamp
			+ '</td><td align="center">'
			+ record.amount
			+ '</td><td class="greenFontBold" align="center">'
			+ record.fromCurrency
			+ '</td><td align="center">'
			+ record.calculatedAmount
			+ '</td><td class="greenFontBold" align="center">'
			+ record.toCurrency 
			+ '</td></tr>'    
			
		}  
		content += "</table>";
		$('#generatedTable').append(content);
		
	}				
				
	function callRecords(){
		$.getJSON("<c:url value="/getrecords" />", showRecords);
	}
		
	function success(data) {
		// Fetch the latest data to be shown as 10 latest
		callRecords();
	}	
	function sendMessage(endpoint, timestamp, amount, fCurr, calculatedAmount,	tCurr) {
		
		$.ajax({
			"type" : 'POST',
			"url" : '<c:url value="/sendrecord" />',
			"data" : JSON.stringify({
				typeOfRequest : endpoint,
				timeStamp : timestamp,
				amount : amount,
				fromCurrency : fCurr,
				calculatedAmount : calculatedAmount,
				toCurrency : tCurr
			}),
			"success" : success,
			"error" : error,
			contentType : "application/json",
			dataType : "json"
		});
	}
	
		
	function error(data) {
		alert("Error HAS happened");
	}

	$(document).ready(function() {
		var showData = $('#tabs-centre-2-result');
		$("#tabs-centre").tabs({
			select : function(event, ui) {
				showData.text("");
			}
		});
	});

	$(function() {
		$('article.tabs').tabs();
	});
	//========================================================	
	$(document)
			.ready(
					function() {

						$("#historical").datepicker({
							dateFormat : 'yy-mm-dd',
							maxDate : new Date(),
							minDate : new Date(1999, 01, 01)
						});
						$('#convert1')
								.click(
										function() {
											$("#dyTable").remove();

											var showData = $('#tabs-centre-2-result');
											showData.text("");
											var historical = $('#historical')
													.val();
											if (historical == "") {
												$('#historical').addClass(
														'borderClass');
												return;
											} else {
												$('#historical').removeClass(
														'borderClass');
											}
											var amount = document.getElementById("amount1").value;
											var sourceCurr = document.getElementById("fromCurrency1");
											var sCurr = sourceCurr.options[sourceCurr.selectedIndex].value;

											var endpoint = "historical";
											// Historical	https://apilayer.net/api/historical?access_key=bc589763e4de333cf044b8c881783651&date=2004-05-11&format=1		
											//console.log("Historical");
											$
													.getJSON(
															"http://apilayer.net/api/"
																	+ endpoint
																	+ "?access_key=bc589763e4de333cf044b8c881783651&currencies=EUR,USD,GBP,NZD,AUD,JPY,HUF,INR&source="
																	+ sCurr
																	+ "&date="
																	+ historical
																	+ "&format=1n",
															function(data) {
																//console.log(data);
																if (data.success == true) {
																	var fromCurr = document.getElementById("fromCurrency1");
																	var fCurr = fromCurr.options[fromCurr.selectedIndex].value;

																	var toCurr = document.getElementById("toCurrency1");
																	var tCurr = toCurr.options[toCurr.selectedIndex].value;

																	var str = fCurr + tCurr;
																//	console.log(str);
																	var jsonVal = "data.quotes";
																	var finalString = jsonVal + "."	+ str;
																	var finalCalculation = eval(finalString);
																	var finalAmount = amount * finalCalculation;
																	//showData.text(parseFloat(Math.round( finalAmount* 10000) / 10000).toFixed(4));
																	var calculatedAmount = parseFloat(Math.round(finalAmount * 10000) / 10000).toFixed(4);
																	var content = "";
																	content = "<table id='dyTable' cellpadding='1em' >"
																	content += '<tr><td>'
																			+ amount
																			+ '</td><td class="greenFontBold">'
																			+ fCurr
																			+ '</td><td>&nbsp; = &nbsp;</td><td>'
																			+ calculatedAmount
																			+ '</td><td class="greenFontBold">'
																			+ tCurr
																			+ '</td></tr>';
																	content += '<tr><td colspan="5" align="center">'
																			+ historical
																			+ '</td></tr>';
																	content += "</table>"
																	$('#tabs-centre-2-result').append(content);
																	// You are always sending record to database
																	 sendMessage(endpoint,historical,amount,fCurr,calculatedAmount,tCurr); 
																} else {
																	alert("Service unavailable");
																}
															}); // end of $.getJSON	  

										}); // end of click
					}); // end of ready function
</script>

<!-- This table is for first time logged-in display or any time page is refreshed  -->
<sec:authorize access="isAuthenticated()">
<div id="generatedTable">
	<table class="offers" >
		<tr class="bold">
			<td width="120px">Type of request</td>
			<td width="200px">Date (ISO format)</td>
			<td width="100px" align="center">Amount</td>
			<td width="100px" align="center">From currency</td>
			<td width="100px" align="center">Amounts to</td>
			<td width="100px" align="center">To currency</td>
		</tr>

		<c:forEach var="record" items="${records}"  begin="0" end="9">
			<tr>
				<td class="request"><c:out value="${record.typeOfRequest}"></c:out></td>
				<td class="name"><c:out value="${record.timeStamp}"></c:out></td>
				<td class="text" align="center"><c:out value="${record.amount}"></c:out></td>
				<td class="greenFontBold" align="center"><c:out value="${record.fromCurrency}"></c:out></td>
				<td class="text" align="center"><c:out value="${record.calculatedAmount}"></c:out></td>
				<td class="greenFontBold" align="center"><c:out value="${record.toCurrency}"></c:out></td>
			</tr>
		</c:forEach>
	
	</table>
</div>
</sec:authorize>