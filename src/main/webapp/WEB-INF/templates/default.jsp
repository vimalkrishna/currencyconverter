<%@ page language="java" contentType="text/html; charset=US-ASCII"
	pageEncoding="US-ASCII"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=US-ASCII">
<title><tiles:insertAttribute name="title"></tiles:insertAttribute></title>

<!-- load CSS in overriding sequence -->
<link href="<c:url value='/resources/css/jquery-ui.css'/>"
	rel="stylesheet" type="text/css" />
<link href="<c:url value='/resources/css/main.css'/>" rel="stylesheet"
	type="text/css" />
<link href="<c:url value='/resources/css/datePicker.css'/>"
	rel="stylesheet" type="text/css" />

<script type="text/javascript"
	src="<c:url value='/resources/script/jquery.min.js'/>"></script>
<script type="text/javascript"
	src="<c:url value='/resources/script/jquery-ui.min.js'/>"></script>


<tiles:insertAttribute name="includes"></tiles:insertAttribute>

</head>
<body>

	<div class="header">
		<tiles:insertAttribute name="header"></tiles:insertAttribute>
	</div>

	<div class="toolbar">
		<tiles:insertAttribute name="toolbar"></tiles:insertAttribute>
	</div>

	<div class="content">
		<tiles:insertAttribute name="content"></tiles:insertAttribute>
	</div>

	<div class="footer">
		<tiles:insertAttribute name="footer"></tiles:insertAttribute>
	</div>


</body>
</html>