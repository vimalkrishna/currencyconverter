package org.vimal.spring.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository("recordsDao")
@Transactional
public class RecordsDao {

	@Autowired
	private SessionFactory sessionfactory;

	public Session getSessionFactory() {
		return sessionfactory.getCurrentSession();
	}

	// GET all Records
	@SuppressWarnings("unchecked")
	
	public List<Record> getRecords() {
		
		System.out.println("getRecords is running...");
		Criteria criteria = getSessionFactory().createCriteria(Record.class);
		return criteria.list();
	}

	// CREATE activity
	public void createOrUpdate(Record record) {
		System.out.println(record);
		getSessionFactory().saveOrUpdate(record);
	}

	// UPDATE activity
	public void update(Record record) {
		getSessionFactory().update(record);
	}

	// DELETE activity
	public boolean delete(int id) {
		Query query = getSessionFactory().createQuery(
				"delete from Record where id=:id");
		query.setLong("id", id);
		return query.executeUpdate() == 1;
	}

	// GET one Record
	public Record getRecord(int id) {
		Criteria criteria = getSessionFactory().createCriteria(Record.class);
		criteria.add(Restrictions.idEq(id));
		return (Record) criteria.uniqueResult();
	}
	
	

}
