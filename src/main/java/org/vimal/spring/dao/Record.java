package org.vimal.spring.dao;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;



@Entity
@Table(name="records")
@XmlRootElement // for JAXB to convert the class
public class Record implements Serializable {

	private static final long serialVersionUID = -2478812101798214419L;

	@Id
	@GeneratedValue
	private int id;
	
	@Column(name="typeofrequest")
	private String typeOfRequest;
	
	@Column(name="fromcurrency")
	private String fromCurrency;
	
	@Column(name="tocurrency")
	private String toCurrency;
	
	private String amount;
	private String calculatedAmount;
	
	@Column(name="timestamp")
	private String timeStamp;
	
	// default constructor, needed by Hibernate
	public Record() { 	}

	public Record(String typeOfRequest, String fromCurrency, String amount, String toCurrency,
			String calculatedAmount, String timeStamp) {
		super();
		this.typeOfRequest = typeOfRequest;
		this.fromCurrency = fromCurrency;
		this.amount = amount;
		this.toCurrency = toCurrency;
		this.calculatedAmount = calculatedAmount;
		this.timeStamp = timeStamp;
	}

	
	public String getCalculatedAmount() {
		return calculatedAmount;
	}

	public void setCalculatedAmount(String calculatedAmount) {
		this.calculatedAmount = calculatedAmount;
	}

	public String getTypeOfRequest() {
		return typeOfRequest;
	}

	public void setTypeOfRequest(String typeOfRequest) {
		this.typeOfRequest = typeOfRequest;
	}

	public String getFromCurrency() {
		return fromCurrency;
	}

	public void setFromCurrency(String fromCurrency) {
		this.fromCurrency = fromCurrency;
	}

	public String getToCurrency() {
		return toCurrency;
	}

	public void setToCurrency(String toCurrency) {
		this.toCurrency = toCurrency;
	}

	public String getAmount() {
		return amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	@Override
	public String toString() {
		return "Record [typeOfRequest=" + typeOfRequest + ", fromCurrency="
				+ fromCurrency + ", toCurrency=" + toCurrency + ", amount="
				+ amount + ", calculatedAmount=" + calculatedAmount
				+ ", timeStamp=" + timeStamp + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		result = prime * result + ((calculatedAmount == null) ? 0 : calculatedAmount.hashCode());
		result = prime * result + ((fromCurrency == null) ? 0 : fromCurrency.hashCode());
		result = prime * result + ((timeStamp == null) ? 0 : timeStamp.hashCode());
		result = prime * result + ((toCurrency == null) ? 0 : toCurrency.hashCode());
		result = prime * result + ((typeOfRequest == null) ? 0 : typeOfRequest.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Record other = (Record) obj;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		if (calculatedAmount == null) {
			if (other.calculatedAmount != null)
				return false;
		} else if (!calculatedAmount.equals(other.calculatedAmount))
			return false;
		if (fromCurrency == null) {
			if (other.fromCurrency != null)
				return false;
		} else if (!fromCurrency.equals(other.fromCurrency))
			return false;
		if (timeStamp == null) {
			if (other.timeStamp != null)
				return false;
		} else if (!timeStamp.equals(other.timeStamp))
			return false;
		if (toCurrency == null) {
			if (other.toCurrency != null)
				return false;
		} else if (!toCurrency.equals(other.toCurrency))
			return false;
		if (typeOfRequest == null) {
			if (other.typeOfRequest != null)
				return false;
		} else if (!typeOfRequest.equals(other.typeOfRequest))
			return false;
		return true;
	}


 

		
	
	
	
}
