package org.vimal.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.vimal.spring.dao.Record;
import org.vimal.spring.dao.RecordsDao;

@Service("recordsService")
public class RecordsService {
	
	@Autowired
	private RecordsDao recordsDao;
	
	public List<Record> getRecords() {
		return recordsDao.getRecords();
	}
	
	public void saveRecord(Record record) {
			recordsDao.createOrUpdate(record);
	}

}
