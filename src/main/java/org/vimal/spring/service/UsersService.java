package org.vimal.spring.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vimal.spring.dao.RecordsDao;
import org.vimal.spring.dao.User;
import org.vimal.spring.dao.UsersDao;

@Service("usersService")
public class UsersService {
	
	@Autowired
	private UsersDao usersDao;
	
	@Autowired
	private RecordsDao recordsDao;
	
	public void create(User user) {
		usersDao.create(user);
	}

	public boolean exists(String username) {
		return usersDao.exists(username);
	}

	public List<User> getAllUsers() {
		return usersDao.getAllUsers();
	}
	
	
	public User getUser(String username) {
		return usersDao.getUser(username);
	}

	
}
