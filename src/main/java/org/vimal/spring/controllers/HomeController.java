package org.vimal.spring.controllers;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import org.vimal.spring.dao.Record;
import org.vimal.spring.service.RecordsService;
import org.vimal.spring.service.UsersService;

@Controller
public class HomeController {
	
	private static Logger logger = Logger.getLogger(HomeController.class);
	// logger.entering(getClass().getName(), "methodName");
	// logger.exiting(getClass().getName(), "methodName");
	// logger.log(Level.SEVERE, "Error doing abc", e);
	
	@Autowired
	private UsersService usersService;

	@Autowired
	private RecordsService recordsService;
	
	@RequestMapping("/")
	public String showHome(Model model, Principal principal) {
		//logger.info("Home called ");
		List<Record> records = recordsService.getRecords();
		Collections.reverse(records);
		model.addAttribute("records", records);
		// logger.info(records);
		return "home";
	}
	

	// @RequestBody GET the data (As JSON) from client 
	@RequestMapping(value="/sendrecord", method=RequestMethod.POST, produces="application/json")
	@ResponseBody
	public Map<String, Object> saveRecord(@RequestBody Map<String,Object> data){
		//logger.info(data);
		
		String typeOfRequest = (String) data.get("typeOfRequest");
		String timeStamp = (String) data.get("timeStamp");
		String amount = (String) data.get("amount");
		String fromCurrency = (String) data.get("fromCurrency");
		String calculatedAmount = (String) data.get("calculatedAmount");
		String toCurrency = (String) data.get("toCurrency");
		logger.info(typeOfRequest+"::"+timeStamp +"::"+amount +"::"+ fromCurrency +"::"+  calculatedAmount  +"::"+ toCurrency );
		
		// Save the single set of data to the Record Table
		Record record = new Record(typeOfRequest, fromCurrency, amount, toCurrency, calculatedAmount, timeStamp);
		recordsService.saveRecord(record);
		Map<String, Object> returnVal = new HashMap<String, Object>();
		returnVal.put("success", true);
		
		return returnVal;
	}
	
	
	// Fetch all data from record table for display (logged-in user).
	@RequestMapping(value="/getrecords", method=RequestMethod.GET, produces="application/json")
	@ResponseBody
	public Map<String, Object> getRecords(Principal principal) {
		
		List<Record> records = null;
		String username=null;
		
		if(principal == null) {
			// Nothing to show to an non logged user, keep it empty initialized.
			// But it can be secured in JSP + with intercept-url pattern
			records = new ArrayList<Record>();
		}
		else {
			username = principal.getName();
			records = recordsService.getRecords();
			//Collections.reverse(Arrays.asList(array));
		}
	// Send back possible values for display and easy calculation for client, as JSON. 
		Map<String, Object> data = new HashMap<String, Object>();
		Collections.reverse(records);
		logger.info("getRecords calls "+data);
		data.put("user", username);
		data.put("records", records);
		data.put("size", records.size());
		
		return data;
	}
	
	
	

}
