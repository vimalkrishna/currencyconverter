package org.vimal.spring.tests;

import static org.junit.Assert.assertEquals;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.vimal.spring.dao.Record;
import org.vimal.spring.dao.RecordsDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = {
		"classpath:org/vimal/spring/config/dao-config.xml",
		"classpath:org/vimal/spring/config/security-config.xml",
		"classpath:org/vimal/spring/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class RecordDaoTest {
	
	@Autowired
	private DataSource dataSource;
	@Autowired
	private RecordsDao recordsDao;
	Record record1 = new Record("live","USD","10","INR","100","2015-5-15");
	Record record2 = new Record("historical","USD","1","INR","5","2010-5-15");
	
	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.execute("delete from records");
	}

	@Test
	public void testCreateRecord() {
		recordsDao.createOrUpdate(record1);
		recordsDao.createOrUpdate(record2);
		List<Record> records = recordsDao.getRecords();
		assertEquals("2 recorsd created ", 2, records.size());
	}
	
	

}
