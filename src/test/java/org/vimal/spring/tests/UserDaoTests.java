package org.vimal.spring.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.List;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import org.vimal.spring.dao.User;
import org.vimal.spring.dao.UsersDao;

@ActiveProfiles("dev")
@ContextConfiguration(locations = {
		"classpath:org/vimal/spring/config/dao-config.xml",
		"classpath:org/vimal/spring/config/security-config.xml",
		"classpath:org/vimal/spring/config/datasource.xml" })
@RunWith(SpringJUnit4ClassRunner.class)
public class UserDaoTests {

	@Autowired
	private UsersDao usersDao;

	@Autowired
	private DataSource dataSource;
	
	private User user1 = new User("vimalkrishna", "Vimal Krishna",
			"vimalkrishna","07-12-1969", "F�hrenweg 9", "Munich", 81549, "Germany", "vimal@gmail.com", true, "ROLE_USER");
	private User user2 = new User("administrator", "The Admin",
			"administrator", "07-12-1969", "F�hrenweg 9","Munich", 81549, "Germany","admin@gmail.com", true,
			"ROLE_ADMIN");
	private User user3 = new User("anyakrishna", "Anya Krishna", "anyakrishna", "07-12-2013",
			"F�hrenweg 9", "Munich", 81549, "Germany","anya@gmail.com", true, "ROLE_USER");
	private User user4 = new User("ananyakrishna", "Ananya Krishna",
			"ananyakrishna", "21-08-2009", "F�hrenweg 9", "Munich", 81549, "Germany","ananya@gmail.com", false,
			"ROLE_USER");
	@Before
	public void init() {
		JdbcTemplate jdbc = new JdbcTemplate(dataSource);
		jdbc.execute("delete from users");
	}
	
	@Test
	public void testCreateRetrieve() {
		usersDao.create(user1);
		
		List<User> users1 = usersDao.getAllUsers();
		
		assertEquals("One user created and retrieved", 1, users1.size());
		assertEquals("Inserted user = match retrieved", user1, users1.get(0));
		
		usersDao.create(user2);
		usersDao.create(user3);
		usersDao.create(user4);
		
		List<User> users2 = usersDao.getAllUsers();
		
		assertEquals("Should be four retrieved users.", 4, users2.size());
	}
	
	@Test
	public void testExists() {
		usersDao.create(user1);
		usersDao.create(user2);
		usersDao.create(user3);
		
		assertTrue("User should exist.", usersDao.exists(user2.getUsername()));
		assertFalse("User should not exist.", usersDao.exists("xkjhsfjlsjf"));
	}
}
